// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0

Canvas {
    id: canvas

    property var lines: []
    property int pencil: 0
    property int highlighter: 1
    property int spray: 2
    property string loadPath: ""

    function addLine(type, color, width, startPoint) {
        var    line = {
            type: type,
            color: color,
            width: width,
            points: startPoint,
        }
        if(type === spray){
            line.points[0].sprayPoints = generateSprayPoints(
                        line.points[0], width)
        }
        lines.push(line)
        requestPaint()
        return line
    }

    function clear() {
        lines = []
        var ctx = getContext("2d")
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        requestPaint()
    }

    function getRandomOffset(radius) {
        var theta = Math.random() * 2 * Math.PI
        var r = Math.sqrt(Math.random()) * radius
        var result = {
            x: Math.cos(theta) * r,
            y: Math.sin(theta) * r
        }
        return  result
    }

    function generateSprayPoints(point, lineWidth){
        const amountOfPoints = 20
        var result = []
        for (var i = 0; i < amountOfPoints; i++) {
            const offset = getRandomOffset(context.lineWidth * 2)
            const resultX = point.x + offset.x
            const resultY = point.y + offset.y
            result.push({x:resultX,
                            y:resultY})
        }
        return result
    }

    onPaint: {
        var ctx = getContext("2d")
        lines.forEach(function (line) {

            ctx.strokeStyle = line.color
            ctx.lineWidth = line.width
            ctx.beginPath()

            switch(line.type){
            case canvas.highlighter:
                ctx.globalCompositeOperation = "multiply"
                ctx.globalAlpha = 0.4
                ctx.lineCap = "butt"
                break
            case canvas.spray:
                line.points.forEach(function (point, iPoint){
                    if(point.sprayPoints.length === 0){
                        point.sprayPoints = generateSprayPoints(point, line.width)
                    }
                })
                break
            default:
                ctx.globalCompositeOperation = "source-over"
                ctx.globalAlpha = 1
                ctx.lineCap = "round"
            }

            if(line.type === canvas.spray){
                line.points.forEach(function (point) {
                    point.sprayPoints.forEach(function (sprayPoint){
                        ctx.fillStyle = ctx.strokeStyle
                        ctx.fillRect(sprayPoint.x,
                                     sprayPoint.y,
                                     1,
                                     1)
                    })
                })
            }else{
                line.points.forEach(function (point, iPoint) {
                    if (iPoint > 0) {
                        ctx.lineTo(point.x, point.y)
                    } else {
                        ctx.moveTo(point.x, point.y)
                    }
                })
                ctx.stroke()
            }
        })
    }
    onLinesChanged: requestPaint()
    onLoadPathChanged: canvas.loadImage(loadPath)
    onImageLoaded: {
        var ctx = getContext("2d")
        ctx.drawImage(loadPath, 0, 0)
        requestPaint()
    }
    onVisibleChanged: {
        if(visible){
            requestPaint()
        }
    }
}
