// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import QtGraphicalEffects 1.0
import Sailfish.Share 1.0

Page {
    id: root

    property string paintColor: "black"
    property int paintWidth: 10
    property int brushType: 0
    property real buttonSize: Math.min(root.width, root.height) / 8
    property real buttonSpacing: Math.min(root.width, root.height) / 40
    property color buttonColor: "light gray"

    objectName: "mainPage"
    allowedOrientations: Orientation.All
    onBrushTypeChanged: {
        switch(brushType) {
        case canvasPage.pencil:
            pencilOverlay.color = Theme.highlightColor
            sprayOverlay.color = Theme.darkPrimaryColor
            highliterOverlay.color = Theme.darkPrimaryColor
            break;
        case canvasPage.spray:
            pencilOverlay.color = Theme.darkPrimaryColor
            sprayOverlay.color = Theme.highlightColor
            highliterOverlay.color = Theme.darkPrimaryColor
            break;
        case canvasPage.highlighter:
            pencilOverlay.color = Theme.darkPrimaryColor
            sprayOverlay.color = Theme.darkPrimaryColor
            highliterOverlay.color = Theme.highlightColor
            break;
        }
    }

    ShareProvider {
        method: "images"
        capabilities: ["image/*"]
        onTriggered: {
            for (var iResource = 0; iResource < resources.length; ++iResource){
                if(resources[iResource].type == ShareResource.FilePathType){
                    canvasPage.loadPath = resources[iResource].filePath
                }
            }
        }
    }

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        title: qsTr("Sketchpad")
        extraContent.children: [
            IconButton {
                objectName: "aboutButton"
                icon.source: "image://theme/icon-m-about"
                anchors.verticalCenter: parent.verticalCenter
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    Image {
        id: setting

        z:2
        anchors.right: parent.right
        anchors.top: pageHeader.bottom
        anchors.rightMargin: 10 * Theme.pixelRatio
        width: buttonSize
        height: width
        source: "image://theme/icon-m-edit"
        MouseArea{
            anchors.fill: parent
            onClicked: {
                panelChooseBrush.open = !panelChooseBrush.open
            }
        }
    }

    CanvasPage {
        id: canvasPage

        anchors.left: panelLeft.right
        anchors.right: parent.right
        anchors.bottom: panelChooseBrush.top
        anchors.top: pageHeader.bottom
    }

    MouseArea {
        property var newLine
        property int type: canvasPage.pencil

        anchors.fill: canvasPage
        onPressed: {
            newLine = canvasPage.addLine(root.brushType,
                                         root.paintColor,
                                         root.paintWidth,
                                         [{ x: mouseX,
                                              y: mouseY,
                                              sprayPoints: []
                                          }]);
        }
        onPositionChanged: {
            newLine.points.push({ x: mouseX,
                                    y: mouseY,
                                    sprayPoints: []});
            canvasPage.requestPaint();
        }
    }

    DockedPanel {
        id: panelChooseBrush

        width: parent.width
        height: Theme.itemSizeExtraLarge + Theme.paddingLarge
        dock: Dock.Bottom
        Row {
            anchors.centerIn: parent
            spacing: buttonSpacing

            Image {
                id: pencil

                source: Qt.resolvedUrl("image://theme/icon-m-edit")
                width: buttonSize
                height: width
                fillMode: Image.PreserveAspectFit
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        brushType = canvasPage.pencil
                    }
                }
                ColorOverlay {
                    id: pencilOverlay

                    anchors.fill: pencil
                    source: pencil
                    color: Theme.highlightColor
                }
            }
            Image {
                id: highliter

                source: Qt.resolvedUrl("../icons/icon-m-highlighter.svg")
                width: buttonSize
                height: width
                fillMode: Image.PreserveAspectFit
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        brushType = canvasPage.highlighter
                    }
                }
                ColorOverlay {
                    id: highliterOverlay

                    anchors.fill: highliter
                    source: highliter
                    color: Theme.darkPrimaryColor
                }
            }
            Image {
                id: spray

                source: Qt.resolvedUrl("../icons/icon-m-spray.svg")
                width: buttonSize
                height: width
                fillMode: Image.PreserveAspectFit
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        sprayOverlay.color =
                        brushType = canvasPage.spray
                    }
                }
                ColorOverlay {
                    id: sprayOverlay

                    anchors.fill: spray
                    source: spray
                    color: Theme.darkPrimaryColor
                }
            }
        }
        onOpenChanged: {
            if(panelLeft.open !== open){
                panelLeft.open = open
            }
        }
    }

    DockedPanel {
        id: panelLeft

        anchors.top: pageHeader.bottom
        anchors.bottom: panelChooseBrush.top
        width: buttonSize + 10 * Theme.pixelRatio
        dock: Dock.Left

        Column {
            id: rightColumn

            spacing: buttonSpacing
            anchors.topMargin: 10 * Theme.pixelRatio
            anchors.fill: parent
            Rectangle {
                width: buttonSize
                height: width
                color: paintColor
                anchors.horizontalCenter: parent.horizontalCenter

                MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        var dialog = pageStack.push("Sailfish.Silica.ColorPickerDialog")
                        dialog.accepted.connect(function() {
                            paintColor = dialog.color
                        })
                    }
                }
            }
            Repeater {
                delegate: Image {
                    width: buttonSize
                    height: width
                    source: modelData
                    anchors.horizontalCenter: parent.horizontalCenter
                    fillMode: Image.PreserveAspectFit
                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: Theme.darkPrimaryColor
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: switch(modelData) {
                                   case "image://theme/icon-m-up":
                                       paintWidth += 5
                                       break
                                   case "image://theme/icon-m-down":
                                       if (paintWidth > 5)
                                           paintWidth -= 5
                                       break
                                   case "image://theme/icon-m-clear":
                                       canvasPage.clear()
                                       break
                                   case "image://theme/icon-m-device-download":
                                       folderPicker.title = qsTr("Save PNG in")
                                       folderPicker.fileName = "picture.png"
                                       pageStack.push(folderPicker)
                                       break
                                   case "image://theme/icon-m-device-upload":
                                       filePicker.title = qsTr("Upload")
                                       pageStack.push(filePicker)
                                       break

                                   case "../icons/icon-m-bmp.png":
                                       folderPicker.title = qsTr("Save BMP in")
                                       folderPicker.fileName = "picture.bmp"
                                       pageStack.push(folderPicker)
                                       break
                                   default:
                                   }
                    }
                }
                model: ["image://theme/icon-m-up",
                    "image://theme/icon-m-down",
                    "image://theme/icon-m-clear",
                    "image://theme/icon-m-device-download",
                    "image://theme/icon-m-device-upload",
                    "../icons/icon-m-bmp.png"
                ]
            }
        }
        onOpenChanged: {
            if(panelChooseBrush.open !== open){
                panelChooseBrush.open = open
            }

        }
    }
    FolderPickerDialog {
        id: folderPicker

        property string fileName: ""
        onAccepted: canvasPage.save(selectedPath + "/" + fileName)
    }
    FilePickerPage {
        id: filePicker

        onSelectedContentChanged: {
            canvasPage.loadPath = selectedContent
        }
    }
}
