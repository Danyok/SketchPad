# Authors
* Evgeniy Samohin, <e.samohin@omp.ru>
  * Developer, 2023
  * Maintainer, 2023  
* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2023
* Konstantin Zvyagin, <k.zvyagin@omp.ru>
  * Reviewer, 2023
* Andrey Tretyakov, <a.tretyakov@omp.ru>
  * Reviewer, 2023
