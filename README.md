# Sketchpad

The project provides an example of using Canvas to draw on the screen.

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md)
documents the rights granted by contributors to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules
of the Open Mobile Platform which informs you how we expect
the members of the community will interact while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).

## Project Structure

The project has a common structure
of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.Sketchpad.pro](ru.auroraos.Sketchpad.pro)** file
  describes the project structure for the qmake build system.
* **[icons](icons)** directory contains application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[icons](qml/icons)** directory contains the custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[Sketchpad.qml](qml/Sketchpad.qml)** file
    provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  **[ru.auroraos.Sketchpad.spec](rpm/ru.auroraos.Sketchpad.spec)** file is used by rpmbuild tool.
  It is generated from **[ru.auroraos.Sketchpad.yaml](rpm/ru.auroraos.Sketchpad.yaml)** file.
* **[src](src)** directory contains the C++ source code.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.Sketchpad.desktop](ru.auroraos.Sketchpad.desktop)** file
  defines the display and parameters for launching the application.
  
## Screenshots
  
  ![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
