# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.Sketchpad

QT += dbus

CONFIG += \
    auroraapp \

PKGCONFIG += \

SOURCES += \
    src/main.cpp \

HEADERS += \

DISTFILES += \
    qml/pages/CanvasPage.qml \
    qml/icons/icon-m-highlighter.svg \
    qml/icons/icon-m-pencil.svg \
    qml/icons/icon-m-spray.png \
    qml/icons/icon-m-bmp.png \
    rpm/ru.auroraos.Sketchpad.spec \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-CLAUSE.md \
    README.md \
    README.ru.md \

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.Sketchpad.ts \
    translations/ru.auroraos.Sketchpad-ru.ts \
